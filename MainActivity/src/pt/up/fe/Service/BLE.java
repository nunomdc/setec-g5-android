package pt.up.fe.Service;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.Handler;
import android.util.Log;

public class BLE {

	private BluetoothAdapter BLEAdapter;
    private Handler BLEHandler;
    protected int rssimed = 0;
    protected int i = 0;
    protected int[] device_rssi = new int[3];
	private long mScanDuration = 0;
	private ArrayList<BLEDevice> mDevices = new ArrayList<BLEDevice>();
	private final WeakReference<BackgroundService> mService;
	protected BLEDevice aux = new BLEDevice();
	protected String auxmac;
	
	public BLE(BluetoothAdapter bLEAdapter, Handler bLEHandler, 
			long scanDuration, BackgroundService service) {
		super();
		BLEAdapter = bLEAdapter;
		BLEHandler = bLEHandler;
		mScanDuration = scanDuration;
		mService = new WeakReference<BackgroundService>(service);
	};

	private BluetoothAdapter.LeScanCallback LeScanCallback =
			new BluetoothAdapter.LeScanCallback() {
		@Override
		public void onLeScan(final BluetoothDevice device, final int rssi,
				final byte[] scanRecord) {
			
		    /*mBLEDeviceAdapter.addDevice(device);
			Log.i("Bluetooth", "Added Device " + String.valueOf(mBLEDeviceAdapter.getCount()) +"\n");		    
			
			Log.i("Bluetooth", device.getName() + " " + device.getAddress() + "\n" + rssi + " dBm");
			Log.i("Bluetooth", scanRecord.toString());*/
			Log.w("Bluetooth"," Scanned Name: "+device.getName()+" MAC: "+device.getAddress()+" rssi: "+rssi);
			aux = new BLEDevice(device.getName(), device.getAddress(), rssi);
			auxmac = aux.getAddress();
			Log.w("Bluetooth", "Is aux in mDevices?: \n" +Boolean.toString(mDevices.contains(aux)) + "\n");
			Log.w("Bluetooth", "Is macaux in mDevices?: \n" +Boolean.toString(mDevices.contains(aux.getAddress())) +"\n");		
			//if(!mDevices.contains(aux/*.equals(device)*/))
				{
					mDevices.add(aux);
					//i = mDevices.indexOf(aux);
					//Log.w("Bluetooth","1st storing RSSI: "+aux.getName()+" MAC: "+aux.getAddress()+" rssi: "+aux.getRssi());
					//Log.w("Bluetooth", "value of i1st?: \n" +Integer.toString(i) +"\n");
				}
			/*else if(mDevices.get(i).equals(aux))
					{	
				mDevices.set(i, aux);
				Log.w("Bluetooth","Update RSSI: "+aux.getName()+" MAC: "+aux.getAddress()+" rssi: "+aux.getRssi());
				Log.w("Bluetooth", "value of i3nd?: \n" +Integer.toString(i) +"\n");
					}*/
			
			/*if(!mDevices.get(1).getAddress().matches(auxmac))
			{
				
				mDevices.add(aux);
				Log.w("Bluetooth","\n" 
						+ "MAC inside element 1: \n" + mDevices.get(1).getAddress() );
				Log.w("Bluetooth","Stored Name: "+aux.getName()+
						" MAC: "+aux.getAddress()+" rssi: "+aux.getRssi());
				i = mDevices.indexOf(aux);
			}	*/
			
		}
		
	};
		
	private Runnable startBLE = new Runnable() {       
	    @Override
	    public void run() {
	    	
	    	mDevices.clear();
	    	Log.e("Bluetooth", ""+mScanDuration);
	    	BLEAdapter.startLeScan(LeScanCallback);
	    	BLEHandler.postDelayed(stopBLE, mScanDuration);
	        
	    }               
	};
	
 	private Runnable stopBLE = new Runnable() {        
	    @Override
	    public void run() {   		
	    	
    	    // Send devices to server
	    	/*
	    	for(int i=0; i<mDevices.size()+1; i++)
	    		{
	    				int rssi =+ device_rssi[i];
	    			if(i==mDevices.size())
	    				rssimed = (rssi/mDevices.size());
	    		}
	    	*/
	    	ArrayList<BLEDevice> newList = averageRssi(mDevices);
    	    mService.get().sendBLEToServer(newList);
    	    mDevices.clear();
    	    BLEAdapter.stopLeScan(LeScanCallback);
    	    BLEAdapter.cancelDiscovery();
	    	
	    	for(int i=0; i<mDevices.size(); i++)
	   		{
	   	        Log.i("Bluetooth", "This is going to BS: Device " + String.valueOf(i+1)  
	   	        		+ " "+ mDevices.get(i).getAddress() + "\n" + mDevices.get(i).getName() 
	  	        		+ "\n"  + mDevices.get(i).getRssi());
	  		}    	
	    }
	};
		
    //Getters and Setters
    
	public Runnable getStartBLE() {
		return startBLE;
	}

	public void setStartBLE(Runnable startBLE) {
		this.startBLE = startBLE;
	}

	public Runnable getStopBLE() {
		return stopBLE;
	}

	public void setStopBLE(Runnable stopBLE) {
		this.stopBLE = stopBLE;
	}
	
	
	public ArrayList<BLEDevice> averageRssi(ArrayList<BLEDevice> mDev)
	{
		ArrayList<String> macs = new ArrayList<String>();
		ArrayList<BLEDevice> newDev = new ArrayList<BLEDevice>();
		int sum=0, count=0;
		
		for(BLEDevice dev : mDev)
		{
			if(!macs.contains(dev.getAddress()))
				macs.add(dev.getAddress());
		}
		
		for(String mac : macs)
		{
			for(BLEDevice dev : mDev)
			{
				if(dev.getAddress().equals(mac))
				{
					sum = sum + dev.getRssi();
					count++;
				}
			}
			newDev.add(new BLEDevice("", mac, (int)(sum/count)));		
		}
		
		return newDev;
	}


}

