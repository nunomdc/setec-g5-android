package pt.up.fe.Presentation;

import java.util.ArrayList;

import com.google.gson.Gson;

import pt.up.fe.Logic.SensorItem;
import pt.up.fe.setec.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class AlarmDialogFragment extends DialogFragment {
	
	private ArrayList<SensorItem> mAlarmSensors = new ArrayList<SensorItem>();
	private String mTitle = "";
	private String mDescription = "";
	private MediaPlayer mMediaPlayer;
	//private SetecApp applicationState;
	
	public AlarmDialogFragment(){
		super();
	}
    
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        
    	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View popupView = inflater.inflate(R.layout.alarm_dialog_fragment, null);
        
        // Get bungle
        Bundle bundle=getArguments();
        ArrayList<String> sensorGson = bundle.getStringArrayList("popup_sensors");
        mTitle = bundle.getString("popup_title", "Aviso");
        mDescription = bundle.getString("popup_description", "Alarme dos aparelhos");
        Gson gson = new Gson();
        for (String data : sensorGson) {
        	SensorItem obj = gson.fromJson(data, SensorItem.class);
        	mAlarmSensors.add(obj);
        }
        
        if( mAlarmSensors != null ) {
        	Log.e("POPUP", "SIZE "+mAlarmSensors.size());
	        // Add sensor to popup to a maximum of 3 sensors
	        if( mAlarmSensors.size() > 0 ){
	        	TextView sensorName = (TextView) popupView.findViewById(R.id.alarmPopupText1);
	            sensorName.setText(mAlarmSensors.get(0).getSensorName());
	            
	            ImageView sensorImage = (ImageView) popupView.findViewById(R.id.alarmPopupImage1);
	            sensorImage.setImageBitmap( BitmapFactory.decodeResource(popupView.getResources(), mAlarmSensors.get(0).getSensorImage()) );
	            
	            popupView.findViewById(R.id.alarmPopupLayout1).setVisibility(View.VISIBLE);
	        }
	        if( mAlarmSensors.size() > 1 ){
	        	TextView sensorName = (TextView) popupView.findViewById(R.id.alarmPopupText2);
	            sensorName.setText(mAlarmSensors.get(1).getSensorName());
	            
	            ImageView sensorImage = (ImageView) popupView.findViewById(R.id.alarmPopupImage2);
	            sensorImage.setImageBitmap( BitmapFactory.decodeResource(popupView.getResources(), mAlarmSensors.get(1).getSensorImage()) );
	            
	            popupView.findViewById(R.id.alarmPopupLayout2).setVisibility(View.VISIBLE);
	        }
	        if( mAlarmSensors.size() > 2 ){
	        	TextView sensorName = (TextView) popupView.findViewById(R.id.alarmPopupText3);
	            sensorName.setText(mAlarmSensors.get(2).getSensorName());
	            
	            ImageView sensorImage = (ImageView) popupView.findViewById(R.id.alarmPopupImage3);
	            sensorImage.setImageBitmap( BitmapFactory.decodeResource(popupView.getResources(), mAlarmSensors.get(2).getSensorImage()) );
	            
	            popupView.findViewById(R.id.alarmPopupLayout3).setVisibility(View.VISIBLE);
	        }
	        
	        // Popup Title
	        ((TextView) popupView.findViewById(R.id.alarmPopupTitle)).setText(mTitle.toUpperCase());
	        ((TextView) popupView.findViewById(R.id.alarmPopupTitle)).setTextColor(getResources().getColor(R.color.text_red));
	        ((TextView) popupView.findViewById(R.id.alarmPopupDescription)).setText(mDescription);
        }
        
        // Start notification
        //Load Bitmap
        Bitmap notificationIcon = BitmapFactory.decodeResource(getResources(),
			R.drawable.ic_launcher);
        
        // Build Intent
        Intent notificationIntent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
        notificationIntent.putExtra("alarm_notification", false);
        notificationIntent.putExtra("alarm_sensors_list", getArguments().getStringArrayList("alarm_sensors_list") );
        PendingIntent pendingIntent = PendingIntent.getActivity(getActivity().getApplicationContext(), 0, notificationIntent, 0);
        
        // Notification Sound
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mMediaPlayer = MediaPlayer.create(getActivity().getApplicationContext(), R.raw.setec_alam);
        mMediaPlayer.setWakeMode(getActivity().getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        /*mMediaPlayer.setOnPreparedListener( new MediaPlayer.OnPreparedListener() {
        	@Override
        	public void onPrepared(MediaPlayer mp) {
        		mp.setLooping(true);
        		mp.start();
        	}
        });
        mMediaPlayer.prepareAsync();*/
        mMediaPlayer.setLooping(true);

		// Build Notification
		Notification serviceNotification = new Notification.Builder(getActivity().getApplicationContext())
	        .setContentTitle("Alerta")
	        .setContentText("Aviso de alerta")
	        .setSmallIcon(R.drawable.ic_launcher)
	        .setLargeIcon(notificationIcon)
	        .setContentIntent(pendingIntent)
	        .setWhen(System.currentTimeMillis())
	        //.setSound(soundUri)
	        .build();
	
		NotificationManager notificationManager = (NotificationManager) getActivity().getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(0, serviceNotification);
    
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(popupView)
        	   // Add action buttons
               .setNeutralButton(R.string.alarm_popup_confirm, new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int id) {
                	   mMediaPlayer.stop();
                	   NotificationManager notificationManager2 = (NotificationManager) getActivity().getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                	   notificationManager2.cancel(0);
                	   AlarmDialogFragment.this.getDialog().cancel();
                   }
               });
        
        return builder.create();
    }
    
}