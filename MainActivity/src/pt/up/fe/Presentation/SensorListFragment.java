package pt.up.fe.Presentation;

import java.util.ArrayList;
import pt.up.fe.Logic.SensorItem;
import pt.up.fe.Logic.SetecApp;
import pt.up.fe.setec.R;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class SensorListFragment extends Fragment{

	/**
	 * During creation, if arguments have been supplied to the fragment
	 * then parse those out.
	 */
	@Override public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	/**
	 * Parse attributes during inflation from a view hierarchy into the
	 * arguments we handle.
	 */
	@Override public void onInflate(Activity activity, AttributeSet attrs,
			Bundle savedInstanceState) {
		super.onInflate(activity, attrs, savedInstanceState);

	}
	
	/**
	 * Create the view for this fragment, using the arguments given to it.
	 */
	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rtnView = inflater.inflate(R.layout.list_nomargin, container, false);
		// Cool ActionBar
		/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			Rect rectgle= new Rect();
			getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(rectgle);
			if(rectgle.top == 0) {
				Log.e("TOP","top: "+rectgle.top);
				rtnView = inflater.inflate(R.layout.list_nomargin, container, false);
			} else  {
				rtnView = inflater.inflate(R.layout.list, container, false);
			}
		} else  {
			rtnView = inflater.inflate(R.layout.list_nomargin, container, false);
		}*/
		//Find ListView in rtnView (self)
		ListView sensorList = (ListView) rtnView.findViewById(R.id.sensor_list);
		
		SensorListAdapter listAdapter = new SensorListAdapter(this.getActivity(),this.getFragmentManager());
		((SetecApp) getActivity().getApplicationContext()).setSensorAdapter(listAdapter);

		//Add dummy data
		ArrayList<SensorItem> displayData = ((SetecApp) getActivity().getApplicationContext()).getSensorsListToDisplay();
		if( displayData != null ){
			listAdapter.setData(displayData);
			sensorList.setAdapter(listAdapter);
		}
		// else error -> get that data!!!
		
		//Return view to display
		return rtnView;
		
	}
}
