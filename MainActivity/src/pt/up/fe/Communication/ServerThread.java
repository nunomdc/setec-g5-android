package pt.up.fe.Communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import pt.up.fe.Logic.SetecApp;
import android.util.Log;

public class ServerThread extends Thread{

    private Socket socket = null;
    private SetecApp mAppState;
    
    public ServerThread(Socket socket, SetecApp appState) {
		super("ServerThread");
		mAppState = appState;
		this.socket = socket;
    }
    
    /*public ServerThread(Socket socket, Context context) {
		super("ServerThread");
		this.context = (ActivityPrototype) context;
		this.socket = socket;
    }*/
 
    public void run() {
        try {
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
 
            String inputLine, outputLine;
            ClientProtocol sp = new ClientProtocol( mAppState );                       
 
            while ((inputLine = in.readLine()) != null) 
            {
            	outputLine = sp.processInput(inputLine);
            	//outputLine = "ola";
                out.println(outputLine);
                Log.w("ServerThread", "Mensagem recebida: " + outputLine);
                if (outputLine.equals("bye"))
                	break;
            }
            out.close();
            in.close();
            socket.close();
 
            } 
            catch (IOException e) {
                System.err.println("ServerThread: falha a ler ou escrever no socket: " + e);
                //System.exit(1);
            }
    }

}
