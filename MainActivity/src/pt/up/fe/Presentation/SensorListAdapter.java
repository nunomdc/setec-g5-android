package pt.up.fe.Presentation;

import java.util.List;
import pt.up.fe.Logic.SensorItem;
import pt.up.fe.Logic.SetecApp;
import pt.up.fe.Messages.SetSensorMessage;
import pt.up.fe.setec.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class SensorListAdapter extends ArrayAdapter<SensorItem>{

	private LayoutInflater mInflater;
	private Activity mContext;
	private SetecApp mApp;
	//private FragmentManager fman;
	
	public SensorListAdapter(Activity context, FragmentManager fm) {
		super(context, android.R.layout.simple_expandable_list_item_1);
		mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mApp = (SetecApp) getContext().getApplicationContext();
		mContext = context;
		//fman = fm;
	}
	
	public void setData(List<SensorItem> data){
		//If new Sensor List received
		if(data != null){
			//Clear Previous Sensor List
			clear();
			//Add new Sensors to List
			for(SensorItem item : data){
				add(item);
			}
		}
	}
	
	/** ViewHolder
	 *  Cache Sensor view to handle recycle
	 */
	private class ViewHolder{
		public TextView sensorDescription;
		public TextView sensorState;
		public Button sensorButton;
		public ImageView sensorImage;
		public ImageView warningImage;
		public SensorItem thisItem;
		public Bitmap sensorImageBitmap;
	}
	
	@Override
	public View getView(int position, View contentView, ViewGroup parent){
		ViewHolder holder;
		final int pos = position;
		
		//If view not initialized
		if(contentView == null){
			
			//Inflate View (Sensor Box)
			contentView = mInflater.inflate(R.layout.sensor_view, null);
			
			// Initialize ViewHolder
			holder = new ViewHolder();
			holder.sensorDescription = (TextView) contentView.findViewById(R.id.sensorDescription);
			holder.sensorState = (TextView) contentView.findViewById(R.id.sensorState);
			holder.sensorImage = (ImageView) contentView.findViewById(R.id.sensorImage);
			holder.sensorButton = (Button) contentView.findViewById(R.id.sensorButton);
		
			holder.warningImage = (ImageView) contentView.findViewById(R.id.image_alarm);
			// Set Click Listener
			holder.sensorButton.setOnClickListener(new View.OnClickListener() {
	             public void onClick(View v) {
	            	 
	            	 if(getItem(pos).isSensorActivated())
	            	 	new TurnOffSensorTask().execute( new Integer[] {getItem(pos).getSensorId(), pos});
	            	 else
	            		new TurnOnSensorTask().execute( new Integer[] {getItem(pos).getSensorId(), pos});
	            	 	
	            	 getItem(pos).setSensorState(!getItem(pos).isSensorActivated());
	            	 //notifyDataSetChanged();
	             }
	        });
			
			// Configure the views
			holder.thisItem = getItem(position);
			
			if( holder.thisItem != null ) { // Load sensor image
				holder.sensorImageBitmap = BitmapFactory.decodeResource(contentView.getResources(), holder.thisItem.getSensorImage() );
			}
						
			//Associate ViewHolder with this View
			contentView.setTag(holder);
		}
		else {
			holder = (ViewHolder) contentView.getTag();
		}
		
		if( holder.thisItem != null ) { //Fail Safe
			// Set sensor name
			holder.sensorDescription.setText( holder.thisItem.getSensorName() );

			// Set sensor image
			holder.sensorImage.setImageBitmap(holder.sensorImageBitmap );
			
			if(holder.thisItem.isSensorAlarmActivated())
				holder.warningImage.setVisibility(View.VISIBLE);
			else
				holder.warningImage.setVisibility(View.GONE);
			
			if(mApp.isUserCaretaker())
				mApp.getSeniorById(mApp.getCurrentSeniorId()).getSensorById(getItem(pos).getSensorId()).setSensorAlarmActivated(false);
			else
				mApp.getUserSeniorData().getSensorById(getItem(pos).getSensorId()).setSensorAlarmActivated(false);
			
			// Check if the sensor state is ON
			if( holder.thisItem.isSensorActivated() ) {
				// Configure Text State
				holder.sensorState.setText( holder.thisItem.getSensorStringStates().getOnState() );
				holder.sensorState.setTextColor(contentView.getResources().getColor(R.color.text_green));
				
				// Configure Button 
				//holder.sensorButton.setVisibility(View.VISIBLE);
				holder.sensorButton.setText( holder.thisItem.getSensorStringStates().getOffVerb() );
			} else {
				// Configure Text State
				holder.sensorState.setText( holder.thisItem.getSensorStringStates().getOffState() );
				holder.sensorState.setTextColor(contentView.getResources().getColor(R.color.text_red));
				
				// Configure Button
				//holder.sensorButton.setVisibility(View.INVISIBLE);
				holder.sensorButton.setText( holder.thisItem.getSensorStringStates().getOnVerb() );
			}
			
			/*if( mApp != null){
				if( mApp.hideother == true){
					if(holder.thisItem.isSensorActivated())
						contentView.setBackgroundColor(contentView.getResources().getColor(R.color.background_grey));
				}
			}*/
		}
		
		return contentView;
	}
	
	
	public class TurnOffSensorTask extends AsyncTask<Integer, Integer, String> {
		protected String doInBackground(Integer... values) {
			String ans=null;
			if(mApp.isUserCaretaker())
			{
				SetSensorMessage ssm = new SetSensorMessage(mApp, mApp.getCurrentSeniorId(), values[0], false);
				ans=ssm.sendMessage();
			}
			else
			{
				SetSensorMessage ssm = new SetSensorMessage(mApp, values[0], false);
				ans=ssm.sendMessage();	
			}
			return ans;
		}

		protected void onPreExecute() {
			mContext.setProgressBarIndeterminateVisibility(true);
		}

		protected void onPostExecute(String result) {
			Log.i("TURNOFFTASK", result);
			mContext.setProgressBarIndeterminateVisibility(false);
			if(result.equals("ok"))
			{
				notifyDataSetChanged();
			}
			else if(result.equals("timeout") || result.equals("prob"))
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        		builder.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	        	   dialog.dismiss();
        	           }
        	       }).setMessage("Impossivel ligar ao servidor.")
        			 .setTitle("Problema de rede")
        			 .create().show();
			}
			else
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        		builder.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	        	   dialog.dismiss();
        	           }
        	       }).setMessage(result)
        			 .setTitle("Erro")
        			 .create().show();
			}
		}
	}
	
	
	
	public class TurnOnSensorTask extends AsyncTask<Integer, Integer, String> {
		protected String doInBackground(Integer... values) {
			String ans=null;
			if(mApp.isUserCaretaker())
			{
				SetSensorMessage ssm = new SetSensorMessage(mApp, mApp.getCurrentSeniorId(), values[0], true);
				ans=ssm.sendMessage();
			}
			else
			{
				SetSensorMessage ssm = new SetSensorMessage(mApp, values[0], true);
				ans=ssm.sendMessage();	
			}
			return ans;
		}

		protected void onPreExecute() {
			mContext.setProgressBarIndeterminateVisibility(true);
		}

		protected void onPostExecute(String result) {
			Log.i("TURNONTASK", result);
			mContext.setProgressBarIndeterminateVisibility(false);
			if(result.equals("ok"))
			{
				notifyDataSetChanged();
			}
			else if(result.equals("timeout") || result.equals("prob"))
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        		builder.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	        	   dialog.dismiss();
        	           }
        	       }).setMessage("Impossivel ligar ao servidor.")
        			 .setTitle("Problema de rede")
        			 .create().show();
			}
			else
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        		builder.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	        	   dialog.dismiss();
        	           }
        	       }).setMessage(result)
        			 .setTitle("Erro")
        			 .create().show();
			}
		}
	}
	
}
