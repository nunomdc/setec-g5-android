package pt.up.fe.Presentation;

import java.util.ArrayList;
import com.google.gson.Gson;
import pt.up.fe.setec.R;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import pt.up.fe.Logic.*;
import pt.up.fe.Messages.LoginMessage;
import pt.up.fe.Messages.LogoutMessage;
import pt.up.fe.Messages.SeniorListMessage;
import pt.up.fe.Messages.SensorListMessage;
import pt.up.fe.Service.BackgroundService;

public class MainActivity extends Activity {

	private SetecApp applicationState;

	//BLE
	private BluetoothAdapter BLEAdapter;
	private static final int REQUEST_ENABLE_BT = 1;
	private static final int REQUEST_PAIRED_DEVICE = 2;
	// END OF BLE

	//	Alarm Handler Vars
	private Handler mHandler; private Gson mGson; 
	private ArrayList<String> mSensorsids; private ArrayList<String> mPopupSensors;
	private int mSeniorid;
	private int mCaretakerid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		applicationState = ((SetecApp) getApplicationContext());

		// Set Initial Content View
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setProgressBarIndeterminateVisibility(false);
		setContentView(R.layout.activity_main);

		// If there is Extras - ex:the case where the activity is called from the alarm
		if( getIntent().getExtras() != null ) {
			// Check if the activity is called from the Alarm Broadcast Receiver
			if( getIntent().getExtras().getBoolean("alarm_notification") ) {
				wakeDevice();
				// Build Bundle
				Bundle extras = new Bundle();
				extras.putStringArrayList("alarm_sensors_list", getIntent().getExtras().getStringArrayList("alarm_sensors_list"));
				extras.putInt("alarm_senior_id", getIntent().getExtras().getInt("alarm_senior_id",-1));
				extras.putInt("alarm_caretaker_id", getIntent().getExtras().getInt("alarm_caretaker_id",-1));
				mHandler = new Handler();
				// Parse inputs
				alarmParser(getIntent().getExtras().getInt("alarm_type",0), extras);
			}
		}


		if( !applicationState.isUserCaretaker() ) {
			// Check for server connection
			boolean serverConnection = true;
			if(serverConnection) {
				BLEAdapter = ((BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
				CheckBlueToothState();

				if( !applicationState.seniorExists() ){
					Senior senior = new Senior("Jonyq",1);
					applicationState.setUserSeniorData(senior);
				}

				//applicationState.getUserSeniorData().setMacAddress(BLEAdapter.getAddress());
				new SeniorLoginNetworkTask().execute(new String[] {BLEAdapter.getAddress()});


			}
		}
		// Is User Logged In // sharedPref.getBoolean(applicationState.prefsIsUserLoggedIn, false)
		else if( applicationState.isUserLoggedIn() && applicationState.isUserCaretaker() ){

			// Show Senior List
			new CaretakerSeniorListLoader().execute();

		}
		// If Not
		else {
			// Show Log In Screen
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.replace(R.id.main_activity_fragment, new LoginFragment(), applicationState.nameLoginFragment).addToBackStack("tag");
			ft.commitAllowingStateLoss();
			if( getActionBar().isShowing() )
				getActionBar().hide();
		}


	}

	private void startAppService() {
		// Establish a connection with the service.  We use an explicit
		// class name because there is no reason to be able to let other
		// applications replace our component.
		startService(new Intent(this, BackgroundService.class));
		bindService(new Intent(this, BackgroundService.class), mConnection, Context.BIND_AUTO_CREATE);
		mIsBound = true;
	}

	private void alarmParser(int alarmType, Bundle extras) {

		switch( alarmType ) {
		case 5:	// atualizacao de estado dos sensores

			break;
		case 6:	// timeout de sensores - senior
			if( extras.getStringArrayList("alarm_sensors_list") != null 
			&& !applicationState.isUserCaretaker()
			&& extras.getInt("alarm_senior_id") != -1) {

				// Get SensorsIds from Alarm Receiver
				mSensorsids = extras.getStringArrayList("alarm_sensors_list");
				mPopupSensors = new ArrayList<String>();
				mGson = new Gson();

				new Thread(new Runnable() {
					public void run() {
						// Update "cache"
						// Senior Sensor List
						new SensorListMessage(applicationState).sendMessage();
						mHandler.post(new Runnable() {
							@Override
							public void run () {
								//wakeDevice();
								for (String id : mSensorsids) {
									SensorItem alarmSensor = applicationState.getUserSeniorData().getSensorById(Integer.parseInt(id));
									applicationState.getUserSeniorData().getSensorById(Integer.parseInt(id)).setSensorAlarmActivated(true);
									if( alarmSensor != null )
										mPopupSensors.add( mGson.toJson(alarmSensor) );
								}

								// Set arguments for popup fragment
								Bundle popupBundle = new Bundle();
								popupBundle.putStringArrayList("alarm_sensors_list", mSensorsids);
								popupBundle.putString("popup_title", getResources().getString(R.string.alarm_popup_title));
								popupBundle.putStringArrayList("popup_sensors", mPopupSensors);
								popupBundle.putString("popup_description", getResources().getString(R.string.alarm_popup_description_timeout));

								//Start Alarm Popup
								FragmentManager fragmentManager = getFragmentManager();
								AlarmDialogFragment newFragment = new AlarmDialogFragment();
								newFragment.setArguments(popupBundle);

								newFragment.show(fragmentManager, "cenas");

								updateUI();
							}
						});
					}
				}).start();	
			}

			break;
		case 7:	// alarme de proximidade - senior
			if( extras.getStringArrayList("alarm_sensors_list") != null 
			&& !applicationState.isUserCaretaker()
			&& extras.getInt("alarm_senior_id") != -1) {

				// Get SensorsIds from Alarm Receiver
				mSensorsids = extras.getStringArrayList("alarm_sensors_list");
				mPopupSensors = new ArrayList<String>();
				mGson = new Gson();

				new Thread(new Runnable() {
					public void run() {
						// Update "cache"
						// Senior Sensor List
						new SensorListMessage(applicationState).sendMessage();
						mHandler.post(new Runnable() {
							@Override
							public void run () {
								for (String id : mSensorsids) {
									SensorItem alarmSensor = applicationState.getUserSeniorData().getSensorById(Integer.parseInt(id));
									applicationState.getUserSeniorData().getSensorById(Integer.parseInt(id)).setSensorAlarmActivated(true);

									if( alarmSensor != null )
										mPopupSensors.add( mGson.toJson(alarmSensor) );
								}
								// Set arguments for popup fragment
								Bundle popupBundle = new Bundle();
								popupBundle.putStringArrayList("alarm_sensors_list", mSensorsids);
								popupBundle.putString("popup_title", getResources().getString(R.string.alarm_popup_title));
								popupBundle.putStringArrayList("popup_sensors", mPopupSensors);
								popupBundle.putString("popup_description", getResources().getString(R.string.alarm_popup_description_proximity));

								//Start Alarm Popup
								FragmentManager fragmentManager = getFragmentManager();
								AlarmDialogFragment newFragment = new AlarmDialogFragment();
								newFragment.setArguments(popupBundle);
								//wakeDevice();
								newFragment.show(fragmentManager, "cenas");

								updateUI();
							}
						});
					}
				}).start();
			}

			break;
		case 8:	// alarme caretaker
		case 9:	// alarme mal funcionamento - caretaker
			if( extras.getStringArrayList("alarm_sensors_list") != null 
			&& applicationState.isUserCaretaker()
			&& extras.getInt("alarm_senior_id") != -1
			&& extras.getInt("alarm_caretaker_id") != -1) {

				// Get SensorsIds from Alarm Receiver
				mSensorsids = extras.getStringArrayList("alarm_sensors_list");
				mPopupSensors = new ArrayList<String>();
				mGson = new Gson();
				mSeniorid = extras.getInt("alarm_senior_id");
				mCaretakerid = extras.getInt("alarm_caretaker_id");
				final int type = alarmType;

				new Thread(new Runnable() {
					public void run() {
						// Update "cache"
						// Seniors list
						new SeniorListMessage(applicationState).sendMessage();
						// Senior Sensor List
						new SensorListMessage(mSeniorid, applicationState).sendMessage();
						mHandler.post(new Runnable() {
							@Override
							public void run () {
								for (String id : mSensorsids) {
									SensorItem alarmSensor = applicationState.getSeniorById(mSeniorid).getSensorById(Integer.parseInt(id));
									applicationState.getSeniorById(mSeniorid).getSensorById(Integer.parseInt(id)).setSensorAlarmActivated(true);
									if( alarmSensor != null )
										mPopupSensors.add( mGson.toJson(alarmSensor) );
								}

								Senior senior = applicationState.getSeniorById(mSeniorid);

								// Set arguments for popup fragment
								Bundle popupBundle = new Bundle();
								popupBundle.putStringArrayList("alarm_sensors_list", mSensorsids);
								popupBundle.putString("popup_title", getResources().getString(R.string.alarm_popup_title));
								popupBundle.putStringArrayList("popup_sensors", mPopupSensors);

								String alarmDescription = "";
								if(type == 8)
									alarmDescription += getResources().getString(R.string.alarm_popup_description_caretaker_global);
								else
									alarmDescription += getResources().getString(R.string.alarm_popup_description_caretaker_error);
								if(senior != null) {
									alarmDescription += senior.getName() + " ";
									alarmDescription += "(" + senior.getAddress() + ")";
								}
								popupBundle.putString("popup_description", alarmDescription);
								//Start Alarm Popup
								FragmentManager fragmentManager = getFragmentManager();
								AlarmDialogFragment newFragment = new AlarmDialogFragment();
								newFragment.setArguments(popupBundle);
								//wakeDevice();
								newFragment.show(fragmentManager, "cenas");	
								updateUI();
							}
						});
					}
				}).run();

			}
			break;
		default:	// erro
			break;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if( !applicationState.isUserCaretaker() )
			getMenuInflater().inflate(R.menu.options_senior_activity, menu);
		else
			getMenuInflater().inflate(R.menu.options_caretaker_activity, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_close:
			finish();
			return true;
		case R.id.action_reload:
			if( applicationState.isUserLoggedIn() && applicationState.isUserCaretaker() )
				new CaretakerSeniorListLoader().execute();
			return true;
		case R.id.action_logout:
			// Detach from service
			if (mIsBound) {
				// If we have received the service, and hence registered with
				// it, then now is the time to unregister.
				if (mService != null) {
					try {
						Message msg = Message.obtain(null,
								BackgroundService.MSG_UNREGISTER_CLIENT);
						msg.replyTo = mMessenger;
						mService.send(msg);
					} catch (RemoteException e) {
						// There is nothing special we need to do if the service
						// has crashed.
					}
				}
				// Detach our existing connection.
				unbindService(mConnection);
				mIsBound = false;
			}
			new LogoutNetworkTask().execute();
			return true;
		default:
			return true;
		}
	}

	public class SeniorLoginNetworkTask extends AsyncTask<String, Integer, String> {	//ask server to login

		protected String doInBackground(String... values) {
			LoginMessage lm;

			if( !values[0].isEmpty() ) {
				lm = new LoginMessage(values[0]);
				//Log.i("UserName", values[0]);
				//Log.i("PassWord", values[1]);
				String ans=null;
				ans = lm.sendMessage( applicationState );
				//ans = "ok";

				Log.i("RESULTADO LOGIN SENIOR", ans);
				return ans;
			}
			else {
				return "FORM_ERROR";
			}
		}
		private void showErrorFragment(String title, String msg){
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			LoadingFragment fg = new LoadingFragment();
			Bundle bundle = new Bundle();
			bundle.putString("title", title);
			bundle.putString("msg", msg);
			fg.setArguments(bundle);
			ft.replace(R.id.main_activity_fragment, fg, applicationState.nameSensorListFragment).addToBackStack("tag");
			ft.commit();
		}

		protected void onPostExecute(String result) {
			if( result != null ) {
				if( result.equals("FORM_ERROR") ) {
					showErrorFragment("BLE","Sem endereço MAC do BLE");
					/*AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
            		builder.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            	           public void onClick(DialogInterface dialog, int id) {
            	        	   dialog.dismiss();
            	        	   finish();
            	           }
            	       }).setMessage("Sem endereço MAC do BLE")
            			 .setTitle("BLE")
            			 .create().show();*/
				}
				else if( result.equals("prob") || result.equals("timeout")) {
					showErrorFragment("Problema de rede","Impossivel ligar ao servidor.");
					Log.i("AsyncTask", "onPostExecute: Completed with an Error.");
					/*AlertDialog.Builder builder = new AlertDialog.Builder();
            		builder.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            	           public void onClick(DialogInterface dialog, int id) {
            	        	   dialog.dismiss();
            	        	   finish();
            	           }
            	       }).setMessage()
            			 .setTitle()
            			 .create().show();*/

				} 
				else if( result.equals("nr"))
				{
					showErrorFragment("Erro","Utilizador não registado");
					/*AlertDialog.Builder builder = new AlertDialog.Builder(getFragmentManager().findFragmentById(R.id.main_activity_fragment).getActivity());
            		builder.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            	           public void onClick(DialogInterface dialog, int id) {
            	        	   dialog.dismiss();
            	        	   finish();
            	           }
            	       }).setMessage("Utilizador não registado")
            			 .setTitle("Erro")
            			 .create().show();*/
				}
				else if( result.equals("err"))
				{
					showErrorFragment("Erro","Erro não esperado.");
					/*AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
            		builder.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            	           public void onClick(DialogInterface dialog, int id) {
            	        	   dialog.dismiss();
            	        	   finish();
            	           }
            	       }).setMessage("Erro não esperado.")
            			 .setTitle("Erro")
            			 .create().show();*/
				}
				else {
					startAppService();
					Log.i("AsyncTask", "onPostExecute: Completed.");

					applicationState.setUserLoggedIn(true);
					new SeniorSensorsLoader().execute();

				}
			}
		}
	}

	/**
	 * Target we publish for clients to send messages to IncomingHandler.
	 */
	final Messenger mMessenger = new Messenger(new IncomingHandler());

	void updateUI(){
		SeniorListFragment seniorFragment = (SeniorListFragment)getFragmentManager().findFragmentByTag("SENIORLISTFRAGMENT");
		SensorListFragment sensorFragment = (SensorListFragment)getFragmentManager().findFragmentByTag("SENSORLISTFRAGMENT");

		if(seniorFragment != null) {
			if(seniorFragment.isVisible() && applicationState.getSeniorAdapter() != null)
				applicationState.getSeniorAdapter().notifyDataSetChanged();
		}
		if(sensorFragment != null) {
			if(sensorFragment.isVisible() && applicationState.getSensorAdapter() != null)
				applicationState.getSensorAdapter().notifyDataSetChanged();
		}

	}

	/**
	 * Handler of incoming messages from service.
	 */
	class IncomingHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case BackgroundService.MSG_DATA_UPDATED:
				super.handleMessage(msg);
				// Reload UI
				Toast.makeText(getApplicationContext(), "RECEIVED "+msg.arg1,  Toast.LENGTH_LONG).show();    	
				updateUI();
				break;
			default:
				super.handleMessage(msg);
			}
		}
	}

	/** Messenger for communicating with service. */
	Messenger mService = null;
	/** Flag indicating whether we have called bind on the service. */
	boolean mIsBound;

	/**
	 * Class for interacting with the main interface of the service.
	 */
	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className,
				IBinder service) {
			// This is called when the connection with the service has been
			// established, giving us the service object we can use to
			// interact with the service.  We are communicating with our
			// service through an IDL interface, so get a client-side
			// representation of that from the raw service object.
			mService = new Messenger(service);

			// We want to monitor the service for as long as we are
			// connected to it.
			try {
				Message msg = Message.obtain(null,
						BackgroundService.MSG_REGISTER_CLIENT);
				msg.replyTo = mMessenger;
				mService.send(msg);

			} catch (RemoteException e) {
				// In this case the service has crashed before we could even
				// do anything with it; we can count on soon being
				// disconnected (and then reconnected if it can be restarted)
				// so there is no need to do anything here.
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected -- that is, its process crashed.
			mService = null;
		}
	};

	public class SeniorSensorsLoader extends AsyncTask<String, Integer, String> {
		protected String doInBackground(String... urls) {

			SensorListMessage sm = new SensorListMessage(applicationState);
			String ans = sm.sendMessage();

			/*
			/*ArrayList<SensorItem> dummyData = new ArrayList<SensorItem>();
			dummyData.add(new SensorItem(0, getResources().getString(R.string.sensor_name_electricity), R.drawable.ferro, true, new StateStrings("ligado", "desligado", "Ligar", "Desligar")));
			dummyData.add(new SensorItem(1, getResources().getString(R.string.sensor_name_humidity), R.drawable.agua, false, new StateStrings("aberta", "fechada", "Abrir", "Fechar")));
			dummyData.add(new SensorItem(2, getResources().getString(R.string.sensor_name_gas), R.drawable.chama, false, new StateStrings("ligado", "desligado", "Ligar", "Desligar")));

			if( getIntent().getExtras() != null ) {
				if( getIntent().getExtras().getBoolean("alarm_notification") ) {
					for (SensorItem sensorItem : applicationState.getPopupSensors()) {
						int index = dummyData.indexOf(sensorItem);
						if( index >= 0){
							dummyData.set(index, sensorItem);
						}
					}
				}
			}
			applicationState.setSensorListToDisplay(dummyData);*/
			/* End of Test Code */

			applicationState.setSensorListToDisplay(applicationState.getUserSeniorData().getSensorList());
			return ans;
		}

		protected void onPreExecute() {
			setProgressBarIndeterminateVisibility(true);
		}

		protected void onPostExecute(String result) {
			setProgressBarIndeterminateVisibility(false);

			if(result.equals("ok"))
			{
				setProgressBarIndeterminateVisibility(false);
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				ft.replace(R.id.main_activity_fragment, new SensorListFragment(), applicationState.nameSensorListFragment).addToBackStack( "tag" );
				ft.commitAllowingStateLoss();
			}
			else if(result.equals("ns"))
			{
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				LoadingFragment fg = new LoadingFragment();
				Bundle bundle = new Bundle();
				bundle.putString("title", "Aviso");
				bundle.putString("msg", "Sem sensores activos");
				fg.setArguments(bundle);
				ft.replace(R.id.main_activity_fragment, fg, applicationState.nameSensorListFragment).addToBackStack("tag") ;
				ft.commit();
			}
			else if(result.equals("prob") || result.equals("timeout"))
			{
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				LoadingFragment fg = new LoadingFragment();
				Bundle bundle = new Bundle();
				bundle.putString("title", "Problema de rede");
				bundle.putString("msg", "Impossivel ligar ao servidor");
				fg.setArguments(bundle);
				ft.replace(R.id.main_activity_fragment, fg, applicationState.nameSensorListFragment).addToBackStack("tag");
				ft.commit();
			}
			else
			{
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				LoadingFragment fg = new LoadingFragment();
				Bundle bundle = new Bundle();
				bundle.putString("title", "Problema de rede");
				bundle.putString("msg", "Impossivel ligar ao servidor");
				fg.setArguments(bundle);
				ft.replace(R.id.main_activity_fragment, fg, applicationState.nameSensorListFragment).addToBackStack("tag");
				ft.commit();

			}
		}
	}

	public class CaretakerSeniorListLoader extends AsyncTask<String, Integer, String> {
		protected String doInBackground(String... urls) {
			String ans = "prob";
			SeniorListMessage sm = new SeniorListMessage(applicationState);
			ans = sm.sendMessage();

			// Fake receive data from server
			/*ArrayList<Senior> dummyData = new ArrayList<Senior>();
			dummyData.add(new Senior("Antonio Maria", 5));
			dummyData.add(new Senior("Joao Souto", 6));
			dummyData.add(new Senior("Ana Gomes", 7));
			dummyData.add(new Senior("Tiago Antunes", 8));
			dummyData.add(new Senior("Maria Albertina", 9));
			dummyData.add(new Senior("Antonio Maria", 5));
			dummyData.add(new Senior("Joao Souto", 6));
			dummyData.add(new Senior("Ana Gomes", 7));
			dummyData.add(new Senior("Tiago Antunes", 8));
			dummyData.add(new Senior("Maria Albertina", 9));
			applicationState.setSeniorList(dummyData);
			ans="ok";*/
			/*
			for (Senior senior : dummyData) {
				senior.addSensor(new SensorItem(0, getResources().getString(R.string.sensor_name_electricity), R.drawable.ferro, true, new StateStrings("ligado", "desligado", "Ligar", "Desligar")));
				senior.addSensor(new SensorItem(1, getResources().getString(R.string.sensor_name_humidity), R.drawable.agua, false, new StateStrings("aberta", "fechada", "Abrir", "Fechar")));
				senior.addSensor(new SensorItem(2, getResources().getString(R.string.sensor_name_gas), R.drawable.chama, false, new StateStrings("ligado", "desligado", "Ligar", "Desligar")));

			}*/
			/*if( getIntent().getExtras() != null ) {
				if( getIntent().getExtras().getBoolean("alarm_notification") ) {
					for (SensorItem sensorItem : applicationState.getPopupSensors()) {
						int index = dummyData.indexOf(sensorItem);
						if( index >= 0){
							dummyData.set(index, sensorItem);
						}
					}
				}
			}*/
			return ans;
		}

		protected void onPreExecute() {
			setProgressBarIndeterminateVisibility(true);
		}

		protected void onPostExecute(String result) {
			setProgressBarIndeterminateVisibility(false);
			if(result.equals("ok"))
			{
				startAppService();
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				ft.replace(R.id.main_activity_fragment, new SeniorListFragment(), applicationState.nameSeniorListFragment).addToBackStack( "tag" );
				ft.commitAllowingStateLoss();
			}
			else if(result.equals("noSeniors"))
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
				builder.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				}).setMessage("Não há seniores para este carataker.")
				.setTitle("Sem seniores")
				.create().show();	
			}
			else
			{

			}
		}
	}

	public class LogoutNetworkTask extends AsyncTask<String, Integer, String> {

		protected String doInBackground(String... params) {
			// Send logout to server
			LogoutMessage logout = new LogoutMessage();
			logout.sendMessage(applicationState);
			return null;
		}

		protected void onPostExecute(String result) {
			// Clear Preferences
			SharedPreferences sharedPref = getSharedPreferences(applicationState.sharedPrefLoginInfo, Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = sharedPref.edit();
			editor.putBoolean(applicationState.prefsIsUserLoggedIn, false);
			editor.putString(applicationState.prefsUserId, "");
			editor.putString(applicationState.prefsUserName, "");
			editor.putBoolean(applicationState.prefsIsUserCaretaker, false);
			editor.commit();

			// Clear App State
			applicationState.setCurrentuserId("");
			applicationState.setCurrentUserName("");
			applicationState.setUserLoggedIn(false);

			// Restart Activity
			recreate();
		}

	}

	//BLE

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == REQUEST_ENABLE_BT){
			CheckBlueToothState();
		}
		if(requestCode == REQUEST_PAIRED_DEVICE){
			if(resultCode == RESULT_OK){
				//Do Work, or not, whatever.
			}           
		}
	}


	private void CheckBlueToothState(){

		if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
			Toast.makeText(this, "BLE Not Supported", Toast.LENGTH_SHORT).show();
			finish();
		}

		if (BLEAdapter == null){
			Log.i("Bluetooth", "Bluetooth NOT enabled");
		}
		else{
			if (BLEAdapter.isEnabled()){

				if(BLEAdapter.isDiscovering()){
					Log.i("Bluetooth", "Bluetooth is currently in device discovery process.");
				}
				else{
					Log.i("Bluetooth", "Bluetooth is Enabled.");
				}
			}
			else{
				Log.i("Bluetooth","Bluetooth is NOT Enabled!");
				Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
			}
		}
	}
	//END OF BLE

	@Override
	protected void onDestroy() {
		super.onDestroy();
		NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancelAll();

		// Detach from service
		if (mIsBound) {
			// If we have received the service, and hence registered with
			// it, then now is the time to unregister.
			if (mService != null) {
				try {
					Message msg = Message.obtain(null,
							BackgroundService.MSG_UNREGISTER_CLIENT);
					msg.replyTo = mMessenger;
					mService.send(msg);
				} catch (RemoteException e) {
					// There is nothing special we need to do if the service
					// has crashed.
				}
			}

			// Detach our existing connection.
			unbindService(mConnection);
			mIsBound = false;
		}
	}

	@TargetApi(19)
	private void setTranslucentStatus(boolean on) {
		Window win = getWindow();
		WindowManager.LayoutParams winParams = win.getAttributes();
		final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
		if (on) {
			winParams.flags |= bits;
		} else {
			winParams.flags &= ~bits;
		}
		win.setAttributes(winParams);
	}

	private void wakeDevice() {
		// Dismiss Keyguard, turn screen on and ask for a wake lock
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
				WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
				WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

	public int getStatusBarHeight() { 
		int result = 0;
		int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
		if (resourceId > 0) {
			result = getResources().getDimensionPixelSize(resourceId);
		} 
		return result;
	} 
}
