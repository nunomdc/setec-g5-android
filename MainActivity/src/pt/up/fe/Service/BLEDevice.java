package pt.up.fe.Service;

public class BLEDevice  {
	private String mName = "";
	private String mAddress = "";
	private int mRssi = 0;
	
	public BLEDevice()
	{
		super();
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(this == obj)
			return true;
		if((obj == null) || (obj.getClass() != this.getClass()))
			return false;
		BLEDevice test = (BLEDevice)obj;
		return mRssi == test.mRssi &&
			(mName == test.mName || (mName != null && mName.equals(test.mName))) &&
			(mAddress == test.mAddress || (mAddress != null && mAddress.equals(test.mAddress)));
		}

	public int hashCode()
	{
		int hash = 7;
		hash = 31 * hash + mRssi;
		hash = 31 * hash + (null == mAddress ? 0 : mAddress.hashCode());
		return hash;
	}
	
	public BLEDevice(String name, String address, int rssi){
		super();
		mName = name;
		mAddress = address;
		mRssi = rssi;
	}

	
	public String getName(){
		return mName;
	}
	
	public String getAddress(){
		return mAddress;
	}
	public int getRssi(){
		return mRssi;
	}

	public void setmRssi(int Rssi) {
		this.mRssi = Rssi;
	}
}