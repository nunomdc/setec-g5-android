package pt.up.fe.Presentation;

import java.util.ArrayList;
import pt.up.fe.Logic.Senior;
import pt.up.fe.Logic.SetecApp;
import pt.up.fe.Messages.SensorListMessage;
import pt.up.fe.setec.R;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SeniorListAdapter extends ArrayAdapter<Senior>{

	private LayoutInflater mInflater;
	private SetecApp mApp;
	private FragmentManager fman;
	private Activity mContext;
	
	public SeniorListAdapter(Activity context, FragmentManager fm) {
		super(context, android.R.layout.simple_expandable_list_item_1);
		mContext = context;
		mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mApp = (SetecApp) getContext().getApplicationContext();
		fman = fm;
	}
	
	public void setData(ArrayList<Senior> data){
		//If new Sensor List received
		if(data != null){
			//Clear Previous Sensor List
			clear();
			//Add new Sensors to List
			for(Senior item : data){
				add(item);
				//new LoadSeniorExtras().execute( data.indexOf(item) );
			}
		}
	}

	/** ViewHolder
	 *  Cache Sensor view to handle recycle
	 */
	private class ViewHolder{
		public TextView seniorName;
		public TextView seniorState;
		public TextView seniorAddress;
		public ImageView seniorAvatar;
		public Bitmap sensorImageBitmap;
		//public View listItemView;	// TODO: for test only ; delete after test with seniors
	}
	
	@Override
	public View getView(final int position, View contentView, ViewGroup parent){
		ViewHolder holder;
		Senior senior = getItem(position);
		
		//If view not initialised
		if(contentView == null){
			//Inflate View (Sensor Box)
			contentView = mInflater.inflate(R.layout.senior_view, null);
			
			// Initialise ViewHolder
			holder = new ViewHolder();
			holder.seniorName = (TextView) contentView.findViewById(R.id.senior_name);
			holder.seniorState = (TextView) contentView.findViewById(R.id.senior_state);
			holder.seniorAddress = (TextView) contentView.findViewById(R.id.senior_address);
			holder.seniorAvatar = (ImageView) contentView.findViewById(R.id.senior_avatar);
			//holder.listItemView = contentView;
			
			// Set Click Listener
			contentView.setOnClickListener(new View.OnClickListener() {
	             public void onClick(View v) {
	            	 new CaretakerSensorsLoader().execute(new String[] {Integer.toString(getItem(position).getId())});
	            	 notifyDataSetChanged();
	             }
	        });
			
			if( senior != null ) { // Load sensor image
				if( senior.getSeniorAvatar() != 0)
					holder.sensorImageBitmap = BitmapFactory.decodeResource(contentView.getResources(), senior.getSeniorAvatar() );
				else
					holder.sensorImageBitmap = BitmapFactory.decodeResource(contentView.getResources(), R.drawable.avatar);
			}
			
			//Associate ViewHolder with this View
			contentView.setTag(holder);
		}
		else {
			holder = (ViewHolder) contentView.getTag();
		}
			
		// Set sensor name
		holder.seniorName.setText( senior.getName());
		holder.seniorState.setText( senior.getActiveWarning() );
		holder.seniorAddress.setText(senior.getAddress());

		// Set sensor image
		holder.seniorAvatar.setImageBitmap(holder.sensorImageBitmap );
		
		return contentView;
	}
	
	
	public class CaretakerSensorsLoader extends AsyncTask<String, Integer, String> {
		protected String doInBackground(String... values) {
			SensorListMessage sm = new SensorListMessage(Integer.parseInt(values[0]), mApp);
			String ans = "";
			ans=sm.sendMessage();
			
			mApp.setCurrentSeniorId(Integer.parseInt(values[0]));
			mApp.setSensorListToDisplay(mApp.getSeniorById(Integer.parseInt(values[0])).getSensorList());

			return ans;
		}

		protected void onPreExecute() {
			mContext.setProgressBarIndeterminateVisibility(true);
		}

		protected void onPostExecute(String result) {
			mContext.setProgressBarIndeterminateVisibility(false);

			if(result.equals("ok"))
			{
				mContext.setProgressBarIndeterminateVisibility(false);
				FragmentTransaction ft = fman.beginTransaction();
				ft.replace(R.id.main_activity_fragment, new SensorListFragment(), "SENSORLISTFRAGMENT").addToBackStack( "tag" );
				ft.commitAllowingStateLoss();
			}
			else if(result.equals("ns"))
			{
				FragmentTransaction ft = fman.beginTransaction();
				LoadingFragment fg = new LoadingFragment();
				Bundle bundle = new Bundle();
				bundle.putString("title", "Aviso");
				bundle.putString("msg", "Sem sensores activos");
				fg.setArguments(bundle);
				ft.replace(R.id.main_activity_fragment, fg, mApp.nameSensorListFragment).addToBackStack("tag");
				ft.commit();
			}
			else if(result.equals("prob") || result.equals("timeout"))
			{
				FragmentTransaction ft = fman.beginTransaction();
				LoadingFragment fg = new LoadingFragment();
				Bundle bundle = new Bundle();
				bundle.putString("title", "Problema de rede");
				bundle.putString("msg", "Impossivel ligar ao servidor");
				fg.setArguments(bundle);
				ft.replace(R.id.main_activity_fragment, fg, mApp.nameSensorListFragment).addToBackStack("tag");
				ft.commit();
			}
			else
			{
				FragmentTransaction ft = fman.beginTransaction();
				LoadingFragment fg = new LoadingFragment();
				Bundle bundle = new Bundle();
				bundle.putString("title", "Problema de rede");
				bundle.putString("msg", "Impossivel ligar ao servidor");
				fg.setArguments(bundle);
				ft.replace(R.id.main_activity_fragment, fg, mApp.nameSensorListFragment).addToBackStack("tag");
				ft.commit();
				
			}
		}
	}
	
	/*private class LoadSeniorExtras extends AsyncTask<Integer, Integer, Integer> {
		private int position = 0;
		private String warning = "";
		private int count = 0;
		
		protected Integer doInBackground(Integer... pos) {
			position = pos[0];
			for( SensorItem sensor : getItem(position).getSensorList() ){
				if( sensor.isSensorAlarmActivated() ) {
					count++;
				}
			}
			return count;
		}
		
		/*protected void onPreExecute() {
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.replace(R.id.main_activity_fragment, new LoadingFragment(), applicationState.nameSensorListFragment);
			//ft.commit();
			ft.commitAllowingStateLoss();
		}/

		protected void onPostExecute(Integer result) {
			Senior senior = getItem(position);
			remove(senior);
			
			if(result==1)
				senior.setActiveWaring("1 alarme activo");
			else if(result>1)
				senior.setActiveWaring(count + " alarmes activos");
			
			insert(senior, position);
			
			notifyDataSetChanged();
			
		}
	}*/
}
