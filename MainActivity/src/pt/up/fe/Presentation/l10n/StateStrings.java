package pt.up.fe.Presentation.l10n;

public class StateStrings {
	private String onState;
	private String offState;
	private String onVerb;
	private String offVerb;
	
	public StateStrings(String onState, String offState, String onVerb,
			String offVerb) {
		super();
		this.onState = onState;
		this.offState = offState;
		this.onVerb = onVerb;
		this.offVerb = offVerb;
	}
	
	public StateStrings() {
		super();
		this.onState = "";
		this.offState = "";
		this.onVerb = "";
		this.offVerb = "";
	}

	public String getOnState() {
		return onState;
	}

	public void setOnState(String onState) {
		this.onState = onState;
	}

	public String getOffState() {
		return offState;
	}

	public void setOffState(String offState) {
		this.offState = offState;
	}

	public String getOnVerb() {
		return onVerb;
	}

	public void setOnVerb(String onVerb) {
		this.onVerb = onVerb;
	}

	public String getOffVerb() {
		return offVerb;
	}

	public void setOffVerb(String offVerb) {
		this.offVerb = offVerb;
	}
	
	
}
