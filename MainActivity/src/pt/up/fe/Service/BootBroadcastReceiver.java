package pt.up.fe.Service;

import pt.up.fe.Logic.SetecApp;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootBroadcastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		
		if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
			Intent serviceIntent = new Intent(context, BackgroundService.class);
			context.startService(serviceIntent);
		}
		else if( intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE") ) {
			if( isMyServiceRunning(context) ) {
	            //Send Broadcast
				context.sendBroadcast(new Intent(SetecApp.CONNECTIVITY_CHANGED));
			} else {
				//Start Activity
				Intent serviceIntent = new Intent(context, BackgroundService.class);
				context.startService(serviceIntent);
			}
		}
	}
	
	private boolean isMyServiceRunning(Context context) {
	    ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if (BackgroundService.class.getName().equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}
}
