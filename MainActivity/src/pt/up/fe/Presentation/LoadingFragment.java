package pt.up.fe.Presentation;

import pt.up.fe.setec.R;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class LoadingFragment extends Fragment{
	
	private String mTitle="";
	private String mMsg="";
	
	/**
	 * During creation, if arguments have been supplied to the fragment
	 * then parse those out.
	 */
	@Override public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mTitle = getArguments().getString("title");
		mMsg = getArguments().getString("msg");
	}
	
	/**
	 * Parse attributes during inflation from a view hierarchy into the
	 * arguments we handle.
	 */
	@Override public void onInflate(Activity activity, AttributeSet attrs,
			Bundle savedInstanceState) {
		super.onInflate(activity, attrs, savedInstanceState);

	}
	
	/**
	 * Create the view for this fragment, using the arguments given to it.
	 */
	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//Inflate sensor list View
		View rtnView = inflater.inflate(R.layout.loading_fragment, container, false);
		((TextView) rtnView.findViewById(R.id.warning_title)).setText(mTitle);
		((TextView) rtnView.findViewById(R.id.warning_msg)).setText(mMsg);
		return rtnView;
		
	}
}
