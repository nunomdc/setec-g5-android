package pt.up.fe.Messages;

import pt.up.fe.Communication.ClientSocket;
import pt.up.fe.Logic.SetecApp;

public class LoginMessage{

	private String username;
	private String passwordMD5;
	private String macAddress;		//used by senior only
	ClientSocket cs=null;
	//private AppController ac = null;
	SetecApp mApplicationState;
	
	public LoginMessage(String username, String passwordMD5)	//used by caretaker only
	{
		super();
		this.username = username;
		this.passwordMD5 = passwordMD5;
		//ac = new AppController();
	}
	
	public LoginMessage(String macAddress)		//used by senior only
	{
		super();
		this.macAddress = macAddress;
	}
		
	public String sendMessage(SetecApp applicationState) {

		// App State
		mApplicationState = applicationState;
		
		String ans="prob";
		String[] ansParts=null;
		cs = new ClientSocket();
		cs.start();
		while(cs.isSocketAlive()==0)
			;
		if(cs.isSocketAlive()==1)
		{
			if( mApplicationState.isUserCaretaker() )
				cs.send("0#C#" + username + "#" + passwordMD5 + "#.");		//caretaker
			else
				cs.send("0#S#" + macAddress + "#.");	//senior
			
			while((ans = cs.read())==null)
			{
			}
			cs.close();
			
			ansParts = ans.split("#");
			
			if(ansParts[0].equals("0") && ansParts[2].equals("0"))
			{
				ans="nr";
			}
			else if(ansParts[0].equals("0"))
			{
				mApplicationState.setCurrentUserName(ansParts[3]);
				mApplicationState.setCurrentuserId(ansParts[2]);
				ans="ok";
				
				//TODO: remove this methods
				//Caretaker.setName(ansParts[3]);
				//Caretaker.setId(Integer.parseInt(ansParts[2]));
			}
			else
			{
				ans="err";
			}
			//ac.updateLoginText(ansParts[3], Integer.parseInt(ansParts[2]), "");
		}
		else if(cs.isSocketAlive()==0)
		{
			//ac.updateLoginText("", 0, "N�o conectado ainda!");
			ans="prob";
		}
		else
		{
			ans="timeout";
			//ac.updateLoginText("", 0, "Timeout!");
		}
		return ans;
	}
}
