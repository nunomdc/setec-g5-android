package pt.up.fe.Presentation;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import pt.up.fe.Logic.SetecApp;
import pt.up.fe.Messages.LoginMessage;
import pt.up.fe.setec.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class LoginFragment extends Fragment {

	EditText username = null;
	EditText password = null;
	TextView resultText = null;
	CheckBox checkBox = null;
	Button b1 = null;
	LinearLayout rl = null;
	private SetecApp applicationState;
	
	/**
	 * During creation, if arguments have been supplied to the fragment
	 * then parse those out.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		applicationState = ((SetecApp) getActivity().getApplicationContext());
	}
	
	/**
	 * Parse attributes during inflation from a view hierarchy into the
	 * arguments we handle.
	 */
	@Override public void onInflate(Activity activity, AttributeSet attrs,
			Bundle savedInstanceState) {
		super.onInflate(activity, attrs, savedInstanceState);

	}
	
	/**
	 * Create the view for this fragment, using the arguments given to it.
	 */
	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate Fragment
		View rtnView = inflater.inflate(R.layout.activity_login, container, false);
		
		// Define Views
		username = (EditText) rtnView.findViewById(R.id.loginUsername);
		password = (EditText) rtnView.findViewById(R.id.loginPassword);
		resultText = (TextView) rtnView.findViewById(R.id.loginTextView1);
		checkBox = (CheckBox) rtnView.findViewById(R.id.checkBox1);
		b1 = (Button) rtnView.findViewById(R.id.loginButton1);
		rl = (LinearLayout) rtnView.findViewById(R.id.loginLinearLayout);
		
		SharedPreferences settings = getActivity().getApplicationContext().getSharedPreferences("Login", 0);
	    String user = settings.getString("Username", null);
	    String pass = settings.getString("Password", null);
	    if(user!=null && pass!=null)
	    {
	    	username.setText(user);
	    	password.setText(pass);
	    	checkBox.setChecked(true);
	    }
		
		b1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(checkBox.isChecked())
				{
					SharedPreferences settings = getActivity().getApplicationContext().getSharedPreferences("Login", 0);
				    SharedPreferences.Editor editor = settings.edit();
				    editor.putString("Username", username.getText().toString());
				    editor.putString("Password", password.getText().toString());
				    editor.commit();
				}
				CaretakerLoginNetworkTask lnt = new CaretakerLoginNetworkTask();
				lnt.execute(new String[] {username.getText().toString(),password.getText().toString()});	
			}
		});
		
		rl.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
			    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
			}
		});
		return rtnView;
		
	}
	
	public class CaretakerLoginNetworkTask extends AsyncTask<String, Integer, String> {	//ask server to login

		LoginMessage lm;
		
		// Do nothing
        /*protected void onPreExecute() {
        	
        }*/

        protected String doInBackground(String...values) { //This runs on a different thread
        	if( !values[0].isEmpty() && !values[0].isEmpty() ) {
				lm = new LoginMessage(username.getText().toString(), md5(password.getText().toString()));
	        	//Log.i("UserName", values[0]);
	        	//Log.i("PassWord", values[1]);
	        	String ans=null;
	        	ans = lm.sendMessage( applicationState );
	        	//ans = "ok";
	        	
	        	Log.i("RESULTADO LOGIN", ans);
				return ans;
        	}
        	else {
        		return "FORM_ERROR";
        	}
        }

        protected void onCancelled() {
            Log.i("AsyncTask", "Cancelled.");
        }
        
        protected void onPostExecute(String result) {
        	if( result != null ) {
        		if( result.equals("FORM_ERROR") ) {
        			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            		builder.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            	           public void onClick(DialogInterface dialog, int id) {
            	        	   dialog.dismiss();
            	           }
            	       }).setMessage(R.string.login_empty_description)
            			 .setTitle(R.string.login_empty_title)
            			 .create().show();
        		}
        		else if( result.equals("prob") || result.equals("timeout")) {
	                Log.i("AsyncTask", "onPostExecute: Completed with an Error.");
	                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            		builder.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            	           public void onClick(DialogInterface dialog, int id) {
            	        	   dialog.dismiss();
            	           }
            	       }).setMessage("Impossivel ligar ao servidor.")
            			 .setTitle("Problema de rede")
            			 .create().show();
	                
	            } 
        		else if( result.equals("nr"))
        		{
        			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            		builder.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            	           public void onClick(DialogInterface dialog, int id) {
            	        	   dialog.dismiss();
            	           }
            	       }).setMessage("Utilizador n�o registado")
            			 .setTitle("Erro")
            			 .create().show();
        		}
        		else if( result.equals("err"))
        		{
        			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            		builder.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            	           public void onClick(DialogInterface dialog, int id) {
            	        	   dialog.dismiss();
            	           }
            	       }).setMessage("Erro inesperado.")
            			 .setTitle("Erro")
            			 .create().show();
        		}
	            else {
	                Log.i("AsyncTask", "onPostExecute: Completed.");
	                
	                applicationState.setUserLoggedIn(true);
	                //applicationState.setCurrentuserId("1234567890");
	                //applicationState.setCurrentUserName("John Doe");
	                
	                boolean saveLoginInfo = true;
	                if( saveLoginInfo ) {
		        		SharedPreferences sharedPref = getActivity().getSharedPreferences(applicationState.sharedPrefLoginInfo, Context.MODE_PRIVATE);
		        		SharedPreferences.Editor editor = sharedPref.edit();
		        		
		        		editor.putBoolean(applicationState.prefsIsUserLoggedIn, applicationState.isUserLoggedIn());
		        		editor.putString(applicationState.prefsUserId, applicationState.getCurrentuserId());
		        		editor.putString(applicationState.prefsUserName, applicationState.getCurrentUserName());
		        		editor.putBoolean(applicationState.prefsIsUserCaretaker, true);
		                editor.commit();
	                }
	                
	                // Fragment Transaction - probably not necessary in the login form
	                /*FragmentTransaction ft = getActivity().getFragmentManager().beginTransaction();
	    			ft.replace(R.id.main_activity_fragment, new LoadingFragment(), applicationState.nameSensorListFragment);
	    			ft.commit();
	    			//ft.commitAllowingStateLoss();*/
	                
	                // Build intent
	                Intent intent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
	                startActivity(intent);
	            }
        	}
        }
    }
	
	public static final String md5(final String s) {
	    final String MD5 = "MD5";
	    try {
	        // Create MD5 Hash
	        MessageDigest digest = java.security.MessageDigest
	                .getInstance(MD5);
	        digest.update(s.getBytes());
	        byte messageDigest[] = digest.digest();

	        // Create Hex String
	        StringBuilder hexString = new StringBuilder();
	        for (byte aMessageDigest : messageDigest) {
	            String h = Integer.toHexString(0xFF & aMessageDigest);
	            while (h.length() < 2)
	                h = "0" + h;
	            hexString.append(h);
	        }
	        return hexString.toString();

	    } catch (NoSuchAlgorithmException e) {
	        e.printStackTrace();
	    }
	    return "";
	}
	
}
