package pt.up.fe.Messages;

import pt.up.fe.Communication.ClientSocket;
import pt.up.fe.Logic.Senior;
import pt.up.fe.Logic.SetecApp;

public class SeniorListMessage{	//used by caretaker only

	private ClientSocket cs=null;
	private SetecApp mAppState;
	
	public SeniorListMessage(SetecApp appState)	
	{		
		super();
		mAppState = appState;
	}
	
	public String sendMessage() {
		
		String ans=null;
		String[] ansParts=null;
		cs = new ClientSocket();
		cs.start();
		while(cs.isSocketAlive()==0)
			;
		if(cs.isSocketAlive()==1)
		{
			//cs.send("1#C#" + Caretaker.getId() + "#.");		//caretaker
			cs.send("1#C#" + mAppState.getCurrentuserId() + "#.");		//caretaker
			while((ans = cs.read())==null)
			{
			}
			cs.close();
			
			ansParts = ans.split("#");
			
			if(ansParts[1].equals("0"))
			{
				return "noSeniors";
			}
			else
			{
				for(int i=1; i<ansParts.length-1; i=i+3)
				{
					mAppState.addSeniorToList(new Senior(ansParts[i+1], ansParts[i+2], Integer.parseInt(ansParts[i])));
					//Caretaker.addSenior(new Senior(ansParts[i+1], ansParts[i+2], Integer.parseInt(ansParts[i])));
				}
				return "ok";
			}
		}
		else if(cs.isSocketAlive()==0)
		{
			//TODO not conected
			return "prob";
		}
		else
		{
			return "timeout";
			//TODO timeout
		}
	}

}
