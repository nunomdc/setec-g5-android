package pt.up.fe.Communication;

import pt.up.fe.Logic.SetecApp;

public class ClientProtocol{

	ClientSocket cs = null;
	SetecApp mAppState;
	//protected ActivityPrototype context;	// nao está a ser usado
	
	public ClientProtocol(SetecApp appState)
	{
		mAppState = appState;
		//this.context = (ActivityPrototype) context;
    }
	
	
	public String processInput(String in)
	{	
		String inParts[]=in.split("#");
		String ans=null;
		
		if(inParts[0].equals("5"))					//used by caretaker
		{
			if(mAppState.isUserCaretaker())
			{
				mAppState.getSeniorById( Integer.parseInt(inParts[2]) )
						 .getSensorById( Integer.parseInt(inParts[1]) )
						 .setSensorState( Integer.parseInt(inParts[3]) == 1 );
				
				ans = "5#" + mAppState.getCurrentuserId() + "#" + inParts[2] + "#" + inParts[1] + "#.";
			}
			else
			{
				mAppState.getUserSeniorData()
				 .getSensorById( Integer.parseInt(inParts[1]) )
				 .setSensorState( Integer.parseInt(inParts[2]) == 1 );
		
				ans = "5#" + mAppState.getCurrentuserId() + "#" + inParts[1] + "#.";
			}
			
			//atualizar estado gr‡fico do sensor
		}
		else if(inParts[0].equals("8"))
		{
			//enviar mensagem em fun�‹o do cancelamento ou n‹o do alarme
		}	
		else if(inParts[0].equals("9"))
		{
			mAppState.getSeniorById( Integer.parseInt(inParts[2]) )
					 .getSensorById( Integer.parseInt(inParts[1]) )
					 .setSensorAlarmActivated(true);
			
			/*Caretaker.getSeniorById(Integer.parseInt(inParts[2]))
				     .getSensorById(Integer.parseInt(inParts[1]))
				     .setProblem(true);*/
			
			ans = "9#" + mAppState.getCurrentuserId() + "#" + inParts[1] + "#" + inParts[2] + "#1#.";
			//ans = "9#" + Caretaker.getId() + "#" + inParts[1] + "#" + inParts[2] + "#1#.";
		}	
		
		
		/*								//used by senior
		if(inParts[0].equals("6"))
		{
			//enviar mensagem em fun�‹o do cancelamento ou n‹o do alarme
		}
		else if(inParts[0].equals("7"))
		{
			//avisar sobre a sa’da de alcance
			ans = "7#" + Senior.getId() + "#" + inParts[1] + "#1#.";
		}
		*/	
		
		return ans;
	}
	
}
