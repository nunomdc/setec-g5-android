package pt.up.fe.Logic;

import java.util.ArrayList;
import java.util.List;
import org.acra.*;
import org.acra.annotation.*;

import pt.up.fe.Presentation.SeniorListAdapter;
import pt.up.fe.Presentation.SensorListAdapter;


import android.app.Application;

@ReportsCrashes(
    formKey = "dDNzZ0pMdzlUVnNqcHI5cjQteG1DbXc6MA"
)
public class SetecApp extends Application {
	
	// Find if is used
	private List<SensorItem> mPopupSensors = new ArrayList<SensorItem>();
	public boolean hideother = false;
	
	// SeniorList - used if user is Caretaker
	private ArrayList<Senior> mSeniorsList = new ArrayList<Senior>();
	// Senior data - used if user is Senior
	private Senior userSeniorData = new Senior();
	// Sensor List to display in fragment
	private ArrayList<SensorItem> mSensorListToDisplay = new ArrayList<SensorItem>();

	// User Data
	private String currentuserId = "";
	private String currentUserName = "";
	private int currentSeniorId = 0;
	private boolean isUserCaretaker = false;	// Set if App Built for Caretaker
	private boolean isUserLoggedIn = false;
	
	//Public Strings
	public String nameSensorListFragment = "SENSORLISTFRAGMENT";
	public String nameSeniorListFragment = "SENIORLISTFRAGMENT";
	public String sharedPrefLoginInfo = "pt.up.fe.LOGIN_SHARED_PREFERENCES";
	public String prefsIsUserCaretaker = "IS_USER_CARETAKER";
	public String prefsIsUserLoggedIn = "IS_USER_LOGGED_IN";
	public String prefsUserId = "LOGIN_USERNAME";
	public String prefsUserName = "LOGIN_PASSWORD";
	private SeniorListAdapter mSeniorAdapter;
	private SensorListAdapter mSensorAdapter;
	public String nameLoginFragment = "LOGINFRAGMENT";
	public static final String CONNECTIVITY_CHANGED = "pt.up.fe.setec.broadcast.service.CONNECTIVITY_CHANGED";
	
	@Override
	public void onCreate(){
		super.onCreate();
		ACRA.init(this);
	}
	
	public List<SensorItem> getPopupSensors() {
		return mPopupSensors;
	}
	public void setPopupSensors(List<SensorItem> popupSensors) {
		mPopupSensors = popupSensors;
	}
	
	/*public List<SensorItem> getSensorsList(){
		return mSensorsList;
	}
	public void setSensorsList(List<SensorItem> sensorsList) {
		mSensorsList = sensorsList;
	}*/
	
	public boolean isUserCaretaker() {
		return isUserCaretaker;
	}

	public String getCurrentuserId() {
		return currentuserId;
	}

	public void setCurrentuserId(String currentuserId) {
		this.currentuserId = currentuserId;
	}

	public String getCurrentUserName() {
		return currentUserName;
	}

	public void setCurrentUserName(String currentUserName) {
		this.currentUserName = currentUserName;
	}
	
	public boolean isUserLoggedIn() {
		return isUserLoggedIn;
	}

	public void setUserLoggedIn(boolean isUserLoggedIn) {
		this.isUserLoggedIn = isUserLoggedIn;
	}
	
	public void addSeniorToList(Senior senior) {
		
		if(isSeniorOnList(senior.getId())==false)
			mSeniorsList.add(senior);
		else {
			int index = seniorIndexList(senior.getId());
			if( index < mSeniorsList.size())
				mSeniorsList.set(index, senior);
		}
	}
	
	public ArrayList<Senior> getSeniorList() {
		return mSeniorsList;
	}
	
	public void setSeniorList( ArrayList<Senior> seniorList ) {
		mSeniorsList = seniorList;
	}
	
	public Senior getSeniorById(int seniorId) {
		
		for (Senior senior : mSeniorsList) {
			if( senior.getId() == seniorId)
				return senior;
		}
		return null;
	}
	
	// TODO Fix this !!!
	public SensorItem getSensorById(int sensorID){
		for (SensorItem sensor : mPopupSensors) {
			if(sensor.getSensorId() == sensorID)
				return sensor;
		}
		return null;
	}
	
	public Senior getUserSeniorData() {
		return userSeniorData;
	}
	
	public boolean seniorExists(){
		return userSeniorData != null;
	}

	public void setUserSeniorData(Senior userSeniorData) {
		this.userSeniorData = userSeniorData;
	}

	public ArrayList<SensorItem> getSensorsListToDisplay() {
		return mSensorListToDisplay;
	}
	
	public void setSensorListToDisplay( ArrayList<SensorItem> sensorList ) {
		mSensorListToDisplay = sensorList;
	}
	
	
	
	public boolean isSeniorOnList(int id) {
		for (Senior senior : mSeniorsList) {
			if(senior.getId() == id)
				return true;
		}
		return false;
	}
	
	public int seniorIndexList(int id) {
		int i = 0;
		for (Senior senior : mSeniorsList) {
			if(senior.getId() == id)
				return i;
			i++;
		}
		return i;
	}
	
	public void setCurrentSeniorId(int id)
	{
		currentSeniorId = id;
	}
	
	public int getCurrentSeniorId()
	{
		return currentSeniorId;
	}

	public void setSeniorAdapter(SeniorListAdapter listAdapter) {
		mSeniorAdapter = listAdapter;
	}
	
	public void setSensorAdapter(SensorListAdapter listAdapter)
	{
		mSensorAdapter = listAdapter;	
	}
	
	public SeniorListAdapter getSeniorAdapter()
	{
		return mSeniorAdapter;
	}
	
	public SensorListAdapter getSensorAdapter()
	{
		return mSensorAdapter;
	}
}
