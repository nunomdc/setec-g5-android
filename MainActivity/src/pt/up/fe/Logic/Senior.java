package pt.up.fe.Logic;

import java.util.ArrayList;


public class Senior {

	private String name = "";
	private String address = "";
	private String activeWarings = "";
	private int id = 0;
	private int mSeniorAvatar = 0;
	//private String macAddress = "";
	private ArrayList<SensorItem> mSensorsList = new ArrayList<SensorItem>();
	
	public Senior(String name, int id)			//Constructor used by senior
	{
		super();
		this.name = name;
		this.id = id;
	}
	
	public Senior(String name, String address, int id)	//Constructor used by caretaker
	{
		this.name = name;
		this.address = address;
		this.id = id;
	}
	
	public Senior() {
	}

	public String getName()
	{
		return name;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	public int getId()
	{
		return id;
	}
	
	/*public void setMacAddress(String mac)
	{
		macAddress = mac;
	}*/	
	
	public void setSensorList( ArrayList<SensorItem> newSensorList ){
		mSensorsList = newSensorList;
	}
	
	public ArrayList<SensorItem> getSensorList() {
		return mSensorsList;
	}
	
	public boolean addSensor(SensorItem sensor)
	{
		if(isSensorOnList(sensor.getSensorId())==false)
			return mSensorsList.add(sensor);
		else
			return false;
	}
	
	public SensorItem getSensorById(int sensorId)
	{
		for (SensorItem sensor : mSensorsList) {
			if( sensor.getSensorId() == sensorId )
				return sensor;
		}
		return null;
	}
	
	public boolean doesSensorExist(String macAddress){
		for (SensorItem sensor : mSensorsList) {
			if( sensor.getmSensorMac().equalsIgnoreCase(macAddress) )
				return true;
		}
		return false;
	}
	
	public int getSensorbyMac(String macAddress){
		for (SensorItem sensor : mSensorsList) {
			if( sensor.getmSensorMac().equalsIgnoreCase(macAddress))
				return sensor.getSensorId();
		}
		return 0;
	}

	public int getSeniorAvatar() {
		return mSeniorAvatar;
	}

	public void setSeniorAvatar(int mSeniorAvatar) {
		this.mSeniorAvatar = mSeniorAvatar;
	}
	
	public void setActiveWaring(String waring) {
		activeWarings = waring;
	}
	
	public String getActiveWarning() {
		return activeWarings;
	}
	
	
	public boolean isSensorOnList(int id) {
		for (SensorItem sensor : mSensorsList) {
			if(sensor.getSensorId() == id)
				return true;
		}
		return false;
	}
	
	public String getSensorsIds()
	{
		String ans = "";
		for(SensorItem item : mSensorsList)
		{
			ans = ans + item.getSensorId() + " ";
		}
		return ans;
	}

}
