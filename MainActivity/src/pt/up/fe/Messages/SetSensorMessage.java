package pt.up.fe.Messages;

import pt.up.fe.Communication.ClientSocket;
import pt.up.fe.Logic.SetecApp;
//import pt.up.fe.Logic.Caretaker;

public class SetSensorMessage{

	private ClientSocket cs=null;
	private int idSensor;
	private boolean activate;
	private int idSenior;	//used by caretaker only
	private SetecApp mAppState;
	
	public SetSensorMessage(SetecApp appState, int idSenior, int idSensor, boolean activate)	//used by caretaker only
	{
		super();
		this.idSensor = idSensor;
		this.idSenior = idSenior;
		this.activate = activate;
		mAppState = appState;
	}
	
	public SetSensorMessage(SetecApp appState, int idSensor, boolean activate)	//used by senior only
	{
		super();
		this.idSensor = idSensor;
		this.activate = activate;
		mAppState = appState;
	}
	
	public String sendMessage() {
		
		String ans="";
		String[] ansParts=null;
		cs = new ClientSocket();
		cs.start();
		while(cs.isSocketAlive()==0)
			;
		if(cs.isSocketAlive()==1)
		{
			if(activate) {
				//cs.send("3#C#" + Caretaker.getId() + "#" + idSensor + "#" + idSenior + "#.");	//caretaker
				//cs.send("3#S#" + Senior.getId() + "#" + idSensor + "#.");	//senior
				
				if( mAppState.isUserCaretaker() )
					cs.send("3#C#" + mAppState.getCurrentuserId() + "#" + idSensor + "#" + idSenior + "#.");	//caretaker
				else
					cs.send("3#S#" + mAppState.getCurrentuserId() + "#" + idSensor + "#.");	//senior
			}
			else {
				//cs.send("4#C#" + Caretaker.getId() + "#" + idSensor + "#" + idSenior + "#.");	//caretaker
				//cs.send("4#S#" + Senior.getId() + "#" + idSensor + "#.");	//senior
				
				if( mAppState.isUserCaretaker() )
					cs.send("4#C#" + mAppState.getCurrentuserId() + "#" + idSensor + "#" + idSenior + "#.");	//caretaker
				else
					cs.send("4#S#" + mAppState.getCurrentuserId() + "#" + idSensor + "#.");	//senior

			}
			while((ans = cs.read())==null)
			{
			}
			cs.close();
			
			ansParts = ans.split("#");
			
			if(ansParts[0].equals("3"))
			{
				if(ansParts[2].equals("0"))
				{
					if(mAppState.isUserCaretaker())
						mAppState.getSeniorById(idSenior).getSensorById(idSensor).setSensorState(false);
					else
						mAppState.getUserSeniorData().getSensorById(idSensor).setSensorState(false);
					ans=ansParts[3];
					//Caretaker.getSeniorById(idSenior).getSensorById(idSensor).setState(0);
				}
				else if(ansParts[2].equals("1"))
				{
					if(mAppState.isUserCaretaker())
						mAppState.getSeniorById(idSenior).getSensorById(idSensor).setSensorState(true);
					else
						mAppState.getUserSeniorData().getSensorById(idSensor).setSensorState(true);
						
					ans="ok";
					//Caretaker.getSeniorById(idSenior).getSensorById(idSensor).setState(1);
				}
			}
			else if(ansParts[0].equals("4"))
			{
				if(ansParts[2].equals("0"))
				{
					if(mAppState.isUserCaretaker())
						mAppState.getSeniorById(idSenior).getSensorById(idSensor).setSensorState(true);
					else
						mAppState.getUserSeniorData().getSensorById(idSensor).setSensorState(true);
					ans = ansParts[3];
					//Caretaker.getSeniorById(idSenior).getSensorById(idSensor).setState(1);
				}
				else if(ansParts[2].equals("1"))
				{
					if(mAppState.isUserCaretaker())
						mAppState.getSeniorById(idSenior).getSensorById(idSensor).setSensorState(false);
					else
						mAppState.getUserSeniorData().getSensorById(idSensor).setSensorState(false);
					ans="ok";
					//Caretaker.getSeniorById(idSenior).getSensorById(idSensor).setState(0);
				}
			}
			//TODO logic
		}
		else if(cs.isSocketAlive()==0)
		{
			//TODO not conected
			ans="prob";
		}
		else
		{
			//TODO timeout
			ans="timeout";
		}
		return ans;
	}

}
