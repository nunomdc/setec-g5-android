package pt.up.fe.Communication;

import java.io.*;
import java.net.*;

import pt.up.fe.Logic.SetecApp;

public class Server extends Thread{
	
	final static private int ACCEPT_PORT = 5001;
	private SetecApp mAppState;
	//protected ActivityPrototype context;
	
	/*public Server(Context context)
	{
		this.context = (ActivityPrototype) context;
	}*/
	
	public Server(SetecApp appState)
	{
		mAppState = appState;
	}
    
    public void run() {
        
        System.out.println("j� corre o thread, run...\n");
        ServerSocket serverSocket = null;
        boolean listening = false;
            
        try {
            serverSocket = new ServerSocket(ACCEPT_PORT);
            System.out.println("ServerSocket aberto na porta: " + ACCEPT_PORT);
            listening = true;
        } 
        catch (IOException e) {
            System.err.println("AppMain: n�o foi poss�vel escutar a porta" + ACCEPT_PORT + ":" + e);
            System.exit(-1);
        }
 
        while (listening){
            try {
                new ServerThread(serverSocket.accept(), mAppState).start();
            } catch (IOException ex) {
                System.err.println("AppMain: erro a criar o ServerThread: " + ex);
                listening=false;
            }
        }
        
        try {
            serverSocket.close();
        } catch (IOException ex) {
            System.err.println("AppMain: erro a fechar o ServerSocket. " + ex);
            System.exit(-1);
        }           
    }
    

}
