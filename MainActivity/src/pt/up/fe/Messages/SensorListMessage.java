package pt.up.fe.Messages;

import java.util.ArrayList;

import android.util.Log;
import pt.up.fe.Communication.ClientSocket;
import pt.up.fe.Logic.SensorItem;
import pt.up.fe.Logic.SetecApp;
import pt.up.fe.Presentation.l10n.StateStrings;
import pt.up.fe.Service.BLEDevice;
import pt.up.fe.setec.R;

public class SensorListMessage{

	private ClientSocket cs=null;
	private int idSenior; //used by caretaker only
	private SetecApp mAppState;
	
	public SensorListMessage(int idSenior, SetecApp appState)	//used by caretaker only
	{
		super();
		this.idSenior = idSenior;
		mAppState = appState;
	}
	
	public SensorListMessage(SetecApp appState)			//used by senior only
	{		
		super();
		mAppState = appState;
	}
	
	public String sendMessage() {
		String ans="ok";
		String[] ansParts=null;
		cs = new ClientSocket();
		cs.start();
		while(cs.isSocketAlive()==0)
			;
		if(cs.isSocketAlive()==1)
		{
			//cs.send("2#C#" + Caretaker.getId() + "#" + idSenior + "#.");	//caretaker
			//cs.send("2#S#" + Senior.getId() + "#."); 			//senior
			if(mAppState.isUserCaretaker()) {
				cs.send("2#C#" + mAppState.getCurrentuserId() + "#" + idSenior + "#.");	//caretaker
			} else {
				cs.send("2#S#" + mAppState.getCurrentuserId() + "#."); 			//senior
			}
			
			while((ans = cs.read())==null)
			{
			}
			cs.close();
			
			ansParts = ans.split("#");
			
			Log.i("SensorListMessage", ans);
			if(mAppState.isUserCaretaker())
			{
				if(ansParts[2].equals("."))
					return "ns"; //no sensors
				else
				{
						
					for(int i=2; i<ansParts.length-1; i=i+3)
					{
						//SensorItem s = new Sensor(Integer.parseInt(ansParts[i]), ansParts[i+1], Integer.parseInt(ansParts[i+2]));
		
						SensorItem sensor;
						
						switch ( Integer.parseInt( ansParts[i+1] ) ) {
							case 1:	// Electric
								sensor = new SensorItem(Integer.parseInt(ansParts[i]),
										 mAppState.getApplicationContext().getResources().getString(R.string.sensor_name_electricity),
										 R.drawable.ferro,
										 Integer.parseInt(ansParts[i+2]) == 1,
										 new StateStrings("ligado", "desligado", "Ligar", "Desligar"));
								break;
							case 2: // Water
								sensor = new SensorItem(Integer.parseInt(ansParts[i]),
										 mAppState.getApplicationContext().getResources().getString(R.string.sensor_name_humidity),
										 R.drawable.agua,
										 Integer.parseInt(ansParts[i+2]) == 1,
										 new StateStrings("aberta", "fechada", "Abrir", "Fechar"));
								break;
							case 3:	// Smoke
								sensor = new SensorItem(Integer.parseInt(ansParts[i]),
										 mAppState.getApplicationContext().getResources().getString(R.string.sensor_name_gas),
										 R.drawable.chama,
										 Integer.parseInt(ansParts[i+2]) == 1,
										 new StateStrings("ligado", "desligado", "Ligar", "Desligar"));
								break;
							default:
								sensor = new SensorItem();
								break;
						}
						
						//SetecApp appState = ((SetecApp) mAppContext.getApplicationContext());
						mAppState.getSeniorById(idSenior).addSensor(sensor);
						return "ok";
						//Caretaker.getSeniorById(idSenior).addSensor(s);
					}
				}
			}
			else
			{
				if(ansParts[1].equals("."))
					return "ns"; //no sensors
				else
				{
					for(int i=1; i<ansParts.length-1; i=i+4)
					{
						//SensorItem s = new Sensor(Integer.parseInt(ansParts[i]), ansParts[i+1], Integer.parseInt(ansParts[i+2]));
						//2#IDc#MAC#Y#Estado
						SensorItem sensor;
						
						switch ( Integer.parseInt( ansParts[i+2] ) ) {
							case 1:	// Electric
								sensor = new SensorItem(Integer.parseInt(ansParts[i]),
										 ansParts[i+1],	
										 mAppState.getApplicationContext().getResources().getString(R.string.sensor_name_electricity),
										 R.drawable.ferro,
										 Integer.parseInt(ansParts[i+3]) == 1,
										 new StateStrings("ligado", "desligado", "Ligar", "Desligar"));
								break;
							case 2: // Water
								sensor = new SensorItem(Integer.parseInt(ansParts[i]),
										 ansParts[i+1],
										 mAppState.getApplicationContext().getResources().getString(R.string.sensor_name_humidity),
										 R.drawable.agua,
										 Integer.parseInt(ansParts[i+3]) == 1,
										 new StateStrings("aberta", "fechada", "Abrir", "Fechar"));
								break;
							case 3:	// Smoke
								sensor = new SensorItem(Integer.parseInt(ansParts[i]),
										 ansParts[i+1],
										 mAppState.getApplicationContext().getResources().getString(R.string.sensor_name_gas),
										 R.drawable.chama,
										 Integer.parseInt(ansParts[i+3]) == 1,
										 new StateStrings("ligado", "desligado", "Ligar", "Desligar"));
								break;
							default:
								sensor = new SensorItem();
								break;
						}
						
						//SetecApp appState = ((SetecApp) mAppContext.getApplicationContext());
						mAppState.getUserSeniorData().addSensor(sensor);
						return "ok";
						//Caretaker.getSeniorById(idSenior).addSensor(s);
					}
				}
			}
			
			
			//TODO logic
		}
		else if(cs.isSocketAlive()==0)
		{
			ans="prob";
			//TODO not conected
		}
		else
		{
			ans="timeout";
			//TODO timeout
		}
		return ans;
	}
	
	public void sendSensors(ArrayList<BLEDevice> devices) {
		if(!mAppState.isUserCaretaker()) {
			// Build String
			String data = "13#S#"+mAppState.getCurrentuserId()+"#";
			for (BLEDevice bleDevice : devices) {
				if(mAppState.getUserSeniorData().doesSensorExist(bleDevice.getAddress())) {
					data += Integer.toString((mAppState.getUserSeniorData().getSensorbyMac(bleDevice.getAddress()))) + "#" 
							+ Integer.toString(bleDevice.getRssi()) + "#";
				}
			}
			Log.w("Bluetooth","Data to send: "+data);
			data += ".";
			// Send String
			
			String ans=null;
			cs = new ClientSocket();
			cs.start();
			while(cs.isSocketAlive()==0)
				;
			if(cs.isSocketAlive()==1)
			{
				cs.send(data);	//caretaker

				// get Reply
				while((ans = cs.read())==null)
				{
				}
				cs.close();
				
				/*ansParts = ans.split("#");
				
				if(ansParts.length > 2){
					ansParts[]
				}*/
			} else if(cs.isSocketAlive()==0)
			{
				//TODO not conected
			}
			else
			{
				//TODO timeout
			}

		}		
	}

}
