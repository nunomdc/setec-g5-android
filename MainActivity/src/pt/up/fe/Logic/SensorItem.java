package pt.up.fe.Logic;

import pt.up.fe.Presentation.l10n.StateStrings;

public class SensorItem {
	
	private String mSensorName;			// Name of the Sensor
	private int mSensorImage;			// Image of the Sensor
	private int mSensorImageAlarm;		// Alarm image of the Sensor
	private boolean mSensorState;		// State of the Sensor aka On / Off
	private boolean mSensorAlarmActivated;	// Set alarm state for the Sensor aka problem
	private String mSensorNameArticle;	// String article for the Sensor
	private StateStrings mSensorStates;	// Strings names for the Sensor 
	private int mSensorId;				// Sensor Id
	private String mSensorMac;			//MAC address of the Sensor
		
	public String getmSensorMac() {
		return mSensorMac;
	}

	public void setmSensorMac(String mSensorMac) {
		this.mSensorMac = mSensorMac;
	}

	/** Default Constructor
	 * @param sensorName
	 * @param sensorImage
	 * @param sensorActivated
	 */
	public SensorItem(int sensorId, String sensorName, int sensorImage,
			boolean sensorActivated) {
		super();
		mSensorName = sensorName;
		mSensorImage = sensorImage;
		mSensorState = sensorActivated;
	}
	
	public SensorItem(int sensorId, String sensorName, int sensorImage,
			boolean sensorActivated, StateStrings sensorStates) {
		super();
		mSensorId = sensorId;
		mSensorName = sensorName;
		mSensorImage = sensorImage;
		mSensorState = sensorActivated;
		mSensorStates = sensorStates;
	}
	
	public SensorItem(int sensorId, String mac, String sensorName, int sensorImage,
			boolean sensorActivated, StateStrings sensorStates) {
		super();
		mSensorId = sensorId;
		mSensorMac = mac;
		mSensorName = sensorName;
		mSensorImage = sensorImage;
		mSensorState = sensorActivated;
		mSensorStates = sensorStates;
	}
	
	/*public SensorItem(String sensorName, int sensorImage,
			boolean sensorActivated, boolean sensorAlarmActivated,
			String sensorNameArticle, StateStrings sensorStates) {
		super();
		this.sensorName = sensorName;
		this.sensorImage = sensorImage;
		this.sensorActivated = sensorActivated;
		this.sensorAlarmActivated = sensorAlarmActivated;
		this.sensorNameArticle = sensorNameArticle;
		this.sensorStates = sensorStates;
	}*/
	
	public SensorItem(String sensorName, int sensorId){
		mSensorName = sensorName;
		mSensorId = sensorId;
	}
	
	/** 
	 *  Empty Constructor
	 */
	public SensorItem() {
		super();
		mSensorName = "";
		mSensorImage = 0;
		mSensorState = false;
		mSensorAlarmActivated = false;
		mSensorNameArticle = "";
	}
	
	/** 
	 * @return Sensor Name 
	 */
	public String getSensorName() {
		return mSensorName;
	}

	/**
	 * @param sensorName
	 */
	public void setSensorName(String sensorName) {
		mSensorName = sensorName;
	}

	/**
	 * @return Image R.id
	 */
	public int getSensorImage() {
		return mSensorImage;
	}

	/**
	 * @param sensorImage Image R.id
	 */
	public void setSensorImage(int sensorImage) {
		mSensorImage = sensorImage;
	}

	/**
	 * @return True if Sensor is in ON state, False otherwise
	 */
	public boolean isSensorActivated() {
		return mSensorState;
	}

	/**
	 * @param sensorActivated Set True if Sensor is in ON state, False otherwise
	 */
	public void setSensorState(boolean sensorActivated) {
		mSensorState = sensorActivated;
	}

	/**
	 * @return True if alarm is active, False otherwise
	 */
	public boolean isSensorAlarmActivated() {
		return mSensorAlarmActivated;
	}

	/**
	 * @param sensorAlarmActivated Set True if alarm is active, False otherwise
	 */
	public void setSensorAlarmActivated(boolean sensorAlarmActivated) {
		mSensorAlarmActivated = sensorAlarmActivated;
	}

	/**
	 * @return Grammatical article for L10n sensor description
	 */
	public String getSensorNameArticle() {
		return mSensorNameArticle;
	}

	/**
	 * @param sensorNameArticle Set grammatical article for L10n sensor description
	 */
	public void setSensorNameArticle(String sensorNameArticle) {
		mSensorNameArticle = sensorNameArticle;
	}

	public StateStrings getSensorStringStates() {
		return mSensorStates;
	}

	public void setSensorStringStates(StateStrings sensorStates) {
		mSensorStates = sensorStates;
	}

	public int getSensorImageAlarm() {
		return mSensorImageAlarm;
	}

	public void setSensorImageAlarm(int sensorImageAlarm) {
		mSensorImageAlarm = sensorImageAlarm;
	}

	public int getSensorId() {
		return mSensorId;
	}

	public void setSensorId(int sensorId) {
		mSensorId = sensorId;
	}

}
